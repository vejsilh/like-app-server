# Like App Server
Like App Server is NodeJs application which provides REST API for simple 
social interaction. Application offers functionalities like signup, 
login and like/unlike other user as well as listing all users with their 
respective liked count.

## Usage
To be able to run application you need to have **NodeJs v8** or later 
installed and **MySQL v5.7** or later.

After cloning the project, you should run `npm install` inside project's 
root folder.

### Setting up database
Create database schema in MySQL server. You can use MySQL Workbench or 
MySQL Command Line Client. You can name the schema as you wish. In this 
guide we'll name it `like_app,` so we can reefer to it later.

### Setting up environment file
In project's root folder, create file named `{environment}.env` where 
`{enivornment}` is application environment. If you want to run app in 
development mode, then you'll create file named `development.env`.
This file must contain all environment related values, like database 
access credentials, application's port, JWT secret, etc.

List of mandatory key-value pairs that `{environment}.env` must contain(in comments you can find example values):

    PORT={app_port}				#5000
    API_ROOT={api_prefix} 			#leave it blank
    JWT_SECRET={jwt_secret}			#MYS3CR3T
    LOG_LEVEL={bunyan_log_level}		#info
    DB_USERNAME={mysql_username}		#root
    DB_PASSWORD={mysql_password}		#root
    DB_SCHEMA={mysql_schema_name}		#like_app
    DB_HOST={mysql_host}			#localhost
    DB_PORT={mysql_port}			#3306 
    DB_DIALECT=mysql			#mysql
    DB_LOGGING={should_log}			#false

### Development mode


To run the app in Development mode(assuming you have `development.env` 
configured), execute these commands in following order:

 - `npm run migrate`  - This will execute database migrations which will 
create all required tables and references between them.
 - `npm run seed`  - Populate database with test data(OPTIONAL).
 - `npm run dev` - Start the Node server.

Now the application is up and running.

### Production mode
To run the app in Development mode(assuming you have `production.env` 
configured), execute these commands in following order:

 - `npm run migrate`  - Execute database migrations which will create 
all required tables and references between them.
 - `npm run prod` - Start the Node server.

## Running Integration Tests
Integration tests use test database, therefore you'll need to create a 
new schema(e.g `like_app_test`) in MySQL which will be used for running 
integration tests.

How to run the tests:
 1. Create environment file named `test.env`.
 2. Populate `test.env` with mandatory key-value pairs(environment 
related variables).
 3. Run `npm run test` command.

