import path from 'path';
import dotenv from 'dotenv';

class Config {
  constructor() {
    dotenv.config({
      path: path.join(__dirname, `/../${process.env.NODE_ENV}.env`),
    });

    this.initializeConfig();
  }

  get env() { return this._env; }

  get root() { return this._root; }

  get port() { return this._port; }

  get ip() { return this._ip; }

  get apiRoot() { return this._apiRoot; }

  get jwtSecret() { return this._jwtSecret; }

  get logLevel() { return this._logLevel; }

  get dbConfig() { return this._dbConfig; }

  initializeConfig() {
    this._env = process.env.NODE_ENV || 'development';
    this._root = path.join(__dirname, '..');
    this._port = process.env.PORT || 9000;
    this._ip = process.env.IP || '0.0.0.0';
    this._apiRoot = process.env.API_ROOT || '';
    this._jwtSecret = process.env.JWT_SECRET;
    this._logLevel = process.env.LOG_LEVEL;

    this._dbConfig = {
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_SCHEMA,
      host: process.env.DB_HOST,
      port: process.env.DB_PORT,
      dialect: process.env.DB_DIALECT,
      logging: process.env.DB_LOGGING === 'true',
    };
  }
}

export default new Config();
