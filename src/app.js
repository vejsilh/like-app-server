import http from 'http';
import config from './config';
import express from './services/express';
import api from './api';
import log from './services/logger';

const app = express(config.apiRoot, api);
const server = http.createServer(app);

setImmediate(() => {
  server.listen(config.port, config.ip, () => {
    log.info({}, `Express server listening on http://${config.ip}:${config.port} in ${config.env} mode`);
  });
});

export default app;
