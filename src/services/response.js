import HelperFunctions from './helpers';

class Response {
  static success(res, entity, status) {
    return res.status(status || 200).json(entity);
  }

  static notFound(res) {
    return res.status(404).end();
  }

  static internalServerError(res, err) {
    const error = HelperFunctions.isCustomError(err) ? err.message : err;
    return res.status(500).json({ error });
  }
}

export default Response;
