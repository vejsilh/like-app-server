import jwt from 'jsonwebtoken';
import config from '../config';

class JwtService {
  static verify(token) {
    return jwt.sign(token, config.jwtSecret);
  }

  static sign(user, options, method = jwt.sign) {
    return method(user, config.jwtSecret, options);
  }
}

export default JwtService;
