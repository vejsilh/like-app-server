import passport from 'passport';
import { BasicStrategy } from 'passport-http';
import { Strategy as JwtStrategy, ExtractJwt } from 'passport-jwt';
import config from '../config';
import UserRepository from '../repositories/user';
import db from '../db/models';
import User from '../db/models/user';

class PassportAuthentication {
  constructor() {
    this.userRepository = new UserRepository(db);
    this.initializeLocalStrategy();
    this.initializeTokenStrategy();
  }

  initializeLocalStrategy() {
    passport.use('password', new BasicStrategy(async (username, password, done) => {
      try {
        const user = await this.userRepository.findByUsername(username);
        const isLegitPassword = await User.authenticate(password, user.password);

        if (isLegitPassword) {
          done(null, user);
        } else {
          done(new Error('Passwords don\'t match'));
        }
      } catch (err) {
        done(err);
      }
    }));
  }

  // eslint-disable-next-line class-methods-use-this
  initializeTokenStrategy() {
    passport.use('token', new JwtStrategy({
      secretOrKey: config.jwtSecret,
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    }, async (user, done) => {
      done(null, user);
    }));
  }

  // eslint-disable-next-line class-methods-use-this
  password(req, res, next) {
    const passportMiddleware = passport.authenticate('password', { session: false }, (err, user) => {
      if (err && err.param) {
        return res.status(400).json(err);
      } if (err || !user) {
        return res.status(401).end();
      }

      return req.logIn(user, { session: false }, (error) => {
        if (error) {
          return res.status(401).end();
        }
        return next();
      });
    });

    passportMiddleware(req, res, next);
  }

  // eslint-disable-next-line class-methods-use-this
  token(req, res, next) {
    const passportMiddleware = passport.authenticate('token', { session: false }, (err, user) => {
      if (err || !user) {
        return res.status(401).end();
      }
      return req.logIn(user, { session: false }, (error) => {
        if (error) {
          return res.status(401).end();
        }
        return next(null, user);
      });
    });

    passportMiddleware(req, res, next);
  }
}

export default new PassportAuthentication();
