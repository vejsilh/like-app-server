class HelperFunctions {
  static isCustomError(err) {
    const customErrorString = '{}';
    return JSON.stringify(err) === customErrorString;
  }
}

export default HelperFunctions;
