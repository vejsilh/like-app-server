import bunyan from 'bunyan';
import config from '../config';
import appInfo from '../../package.json';

class Logger {
  constructor(logger) {
    this._log = logger.createLogger({
      name: appInfo.name,
      level: config.logLevel,
      stream: process.stdout,
      serializers: {
        req: logger.stdSerializers.req,
        res: bunyan.stdSerializers.res,
        err: bunyan.stdSerializers.err,
      },
    });
  }

  error(logObj, message) {
    this._log.error(logObj, message);
  }

  warn(logObj, message) {
    this._log.warn(logObj, message);
  }

  info(logObj, message) {
    this._log.info(logObj, message);
  }
}

const log = new Logger(bunyan);
export default log;
