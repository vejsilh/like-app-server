import request from 'supertest';
import express from '../../services/express';
import routes from '.';
import config from '../../config';
import JwtService from '../../services/jwt';

const app = () => express(config.apiRoot, routes);

const TEST_USER = {
  firstName: 'John',
  lastName: 'Doe',
  username: 'testuser',
  password: 'password',
  confirmPassword: 'password',
};

describe('POST /signup', () => {
  test('successfull sign up', async () => {
    const { status, body } = await request(app())
      .post(`${config.apiRoot}/signup`)
      .set('Accept', 'application/json')
      .send(TEST_USER);

    expect(status).toBe(200);
    expect(typeof body).toBe('object');
    expect(typeof body.token).toBe('string');
    expect(typeof body.user).toBe('object');
  });

  test('username taken', async () => {
    const { status, body } = await request(app())
      .post(`${config.apiRoot}/signup`)
      .set('Accept', 'application/json')
      .send(TEST_USER);

    expect(status).toBe(500);
    expect(typeof body).toBe('object');
    expect(typeof body.error).toBe('object');
    expect(typeof body.error.name).toBe('string');
    expect(body.error.name).toEqual('SequelizeUniqueConstraintError');
  });

  test('passwords not equal', async () => {
    const testUser = { ...TEST_USER, password: 'differentpass' };
    const { status, body } = await request(app())
      .post(`${config.apiRoot}/signup`)
      .set('Accept', 'application/json')
      .send(testUser);

    expect(status).toBe(400);
    expect(typeof body).toBe('object');
    expect(typeof body.statusCode).toBe('number');
    expect(typeof body.message).toBe('string');
  });
});

describe('POST /login', () => {
  test('successfull login', async () => {
    const { status, body } = await request(app())
      .post(`${config.apiRoot}/login`)
      .set({
        Accept: 'application/json',
        Authorization: `Basic ${Buffer.from(`${TEST_USER.username}:${TEST_USER.password}`).toString('base64')}`,
      })
      .send(TEST_USER);

    expect(status).toBe(200);
    expect(typeof body).toBe('object');
    expect(typeof body.token).toBe('string');
    expect(typeof body.user).toBe('object');
    expect(await JwtService.verify(body.token)).toBeTruthy();
  });

  test('failed login with wrong password', async () => {
    const wrongPassword = 'notrealpassword';
    const { status } = await request(app())
      .post(`${config.apiRoot}/login`)
      .set({
        Accept: 'application/json',
        Authorization: `Basic ${Buffer.from(`${TEST_USER.username}:${wrongPassword}`).toString('base64')}`,
      })
      .send(TEST_USER);

    expect(status).toBe(401);
  });

  test('failed login user does not exist', async () => {
    const wrongUsername = 'notrealuser';
    const { status } = await request(app())
      .post(`${config.apiRoot}/login`)
      .set({
        Accept: 'application/json',
        Authorization: `Basic ${Buffer.from(`${wrongUsername}:${TEST_USER.password}`).toString('base64')}`,
      })
      .send(TEST_USER);

    expect(status).toBe(401);
  });
});
