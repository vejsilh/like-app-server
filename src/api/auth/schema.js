import Joi from 'joi';

class AuthSchema {
  constructor() {
    this.initializeSchema();
  }

  get signUp() {
    return this._signup;
  }

  initializeSchema() {
    this._signup = {
      body: {
        username: Joi.string().required().min(2).max(50),
        firstName: Joi.string().required().min(2).max(50),
        lastName: Joi.string().required().min(2).max(50),
        password: Joi.string().required().regex(/^[a-zA-Z0-9]{3,30}$/),
        confirmPassword: Joi.string().required().regex(/^[a-zA-Z0-9]{3,30}$/).valid(Joi.ref('password')),
      },
    };
  }
}

export default new AuthSchema();
