import { Router } from 'express';
import expressJoi from 'express-joi-validator';
import AuthController from './controller';
import UserRepository from '../../repositories/user';
import schema from './schema';
import db from '../../db/models';
import PassportAuthentication from '../../services/passport';

const router = new Router();
const userRepository = new UserRepository(db);
const authController = new AuthController(userRepository);

router.post(
  '/signup',
  expressJoi(schema.signUp),
  authController.signUp,
);

router.post(
  '/login',
  PassportAuthentication.password,
  authController.login,
);

export default router;
