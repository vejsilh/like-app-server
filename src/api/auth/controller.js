import JwtService from '../../services/jwt';
import Response from '../../services/response';
import log from '../../services/logger';

export default class AuthController {
  constructor(userRepository) {
    this.userRepository = userRepository;
  }

   signUp = async (req, res) => {
     try {
       const user = await this.userRepository.create(req.body);
       const token = JwtService.sign({ ...user.view() });
       const response = {
         user: user.view(),
         token,
       };

       log.info({ req, response }, 'New user signed up');
       return Response.success(res, response);
     } catch (err) {
       log.error({ req, err }, 'Signup failed!');
       return Response.internalServerError(res, err);
     }
   }

   login = async (req, res) => {
     const { user } = req;
     const token = JwtService.sign({ ...user.view() });
     const response = {
       user: user.view(),
       token,
     };

     log.info({ req, response }, 'User loged in');
     return Response.success(res, response);
   }
}
