import { Router } from 'express';
import expressJoi from 'express-joi-validator';
import UserRepository from '../../repositories/user';
import LikeRepository from '../../repositories/like';
import UserController from './controller';
import PassportAuthentication from '../../services/passport';
import schema from './schema';
import db from '../../db/models';

const router = new Router();
const userRepository = new UserRepository(db);
const likeRepository = new LikeRepository(db);
const userController = new UserController(userRepository, likeRepository);

router.get(
  '/me',
  PassportAuthentication.token,
  userController.me,
);

router.put(
  '/me/update-password',
  expressJoi(schema.updatePassword),
  PassportAuthentication.token,
  userController.updatePassword,
);

router.put(
  '/user/:id/like',
  expressJoi(schema.likeUser),
  PassportAuthentication.token,
  userController.likeUser,
);

router.put(
  '/user/:id/unlike',
  expressJoi(schema.unlikeUser),
  PassportAuthentication.token,
  userController.unlikeUser,
);

router.get(
  '/user/:id',
  expressJoi(schema.likesInfo),
  userController.likesInfo,
);

router.get(
  '/most-liked',
  expressJoi(schema.mostLiked),
  userController.mostLiked,
);

export default router;
