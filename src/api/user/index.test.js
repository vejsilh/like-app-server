import request from 'supertest';
import express from '../../services/express';
import routes from '.';
import config from '../../config';
import db from '../../db/models';
import JwtService from '../../services/jwt';

const app = () => express(config.apiRoot, routes);

let userJwtToken = null;
let testUser = null;
let likedUser = null;
beforeAll(async () => {
  testUser = {
    firstName: 'Sabina',
    lastName: 'Monroe',
    username: 'sabinamo',
    password: 'testtest',
    likedCount: 0,
  };
  const user = await db.User.create(testUser);
  userJwtToken = JwtService.sign({ ...user.view() });
  testUser.id = user.id;
});

describe('GET /me', () => {
  test('returns currently logged in user\'s info', async () => {
    const { status, body } = await request(app())
      .get(`${config.apiRoot}/me`)
      .set({ Accept: 'application/json', Authorization: `Bearer ${userJwtToken}` })
      .send();

    expect(status).toBe(200);
    expect(typeof body).toBe('object');
    expect(body).toEqual({ ...testUser, password: undefined });
  });

  test('returns unathorized for user without token', async () => {
    const { status, body } = await request(app())
      .get(`${config.apiRoot}/me`)
      .set({ Accept: 'application/json' })
      .send();

    expect(status).toBe(401);
    expect(body).toEqual({});
  });
});

describe('PUT /me/update-password', () => {
  test('successfully updates password', async () => {
    const passwordUpdatePayload = {
      currentPassword: testUser.password,
      newPassword: 'thisisnewpassword',
      newPasswordConfirm: 'thisisnewpassword',
    };

    const { status, body } = await request(app())
      .put(`${config.apiRoot}/me/update-password`)
      .set({ Accept: 'application/json', Authorization: `Bearer ${userJwtToken}` })
      .send(passwordUpdatePayload);

    expect(status).toBe(200);
    expect(typeof body).toBe('object');
    expect(typeof body.message).toBe('string');
    expect(body.message).toEqual('Password sucessufuly changed');

    testUser.password = passwordUpdatePayload.password;
  });

  test('failed because of illegal characters in password', async () => {
    const passwordUpdatePayload = {
      currentPassword: testUser.password,
      newPassword: 'this_is_%=$!newpassword',
      newPasswordConfirm: 'this_is_%=$!newpassword',
    };

    const { status, body } = await request(app())
      .put(`${config.apiRoot}/me/update-password`)
      .set({ Accept: 'application/json', Authorization: `Bearer ${userJwtToken}` })
      .send(passwordUpdatePayload);

    expect(status).toBe(400);
    expect(typeof body).toBe('object');
    expect(body).toHaveProperty('statusCode', 'error', 'message');
  });
});

describe('PUT /user/:id/like', () => {
  beforeAll(async () => {
    likedUser = {
      firstName: 'Merlin',
      lastName: 'Jackson',
      username: 'merlin',
      password: 'merlinmerlin',
    };
    const userA = await db.User.create(likedUser);
    likedUser.id = userA.id;
  });

  test('successfully likes user', async () => {
    const { status, body } = await request(app())
      .put(`${config.apiRoot}/user/${likedUser.id}/like`)
      .set({ Accept: 'application/json', Authorization: `Bearer ${userJwtToken}` })
      .send();

    expect(status).toBe(200);
    expect(typeof body).toBe('object');
    expect(typeof body.message).toBe('string');
    expect(body.message).toEqual('Sucessufuly liked user');
  });

  test('unable to like - user already liked', async () => {
    const { status, body } = await request(app())
      .put(`${config.apiRoot}/user/${likedUser.id}/like`)
      .set({ Accept: 'application/json', Authorization: `Bearer ${userJwtToken}` })
      .send();

    expect(status).toBe(500);
    expect(typeof body).toBe('object');
    expect(typeof body.error).toBe('string');
    expect(body.error).toEqual('You already liked this user');
  });

  test('unable to like non existent user', async () => {
    const nonExistentUserId = 0;
    const { status, body } = await request(app())
      .put(`${config.apiRoot}/user/${nonExistentUserId}/like`)
      .set({ Accept: 'application/json', Authorization: `Bearer ${userJwtToken}` })
      .send();

    expect(status).toBe(500);
    expect(typeof body).toBe('object');
    expect(typeof body.error).toBe('object');
  });

  test('unable to like yourself', async () => {
    const { status, body } = await request(app())
      .put(`${config.apiRoot}/user/${testUser.id}/like`)
      .set({ Accept: 'application/json', Authorization: `Bearer ${userJwtToken}` })
      .send();

    expect(status).toBe(500);
    expect(typeof body).toBe('object');
    expect(typeof body.error).toBe('string');
    expect(body.error).toEqual('You cannot like yourself!');
  });
});

describe('PUT /user/:id/unlike', () => {
  test('successfully unlikes user', async () => {
    const { status, body } = await request(app())
      .put(`${config.apiRoot}/user/${likedUser.id}/unlike`)
      .set({ Accept: 'application/json', Authorization: `Bearer ${userJwtToken}` })
      .send();

    expect(status).toBe(200);
    expect(typeof body).toBe('object');
    expect(typeof body.message).toBe('string');
    expect(body.message).toEqual('Sucessufuly unliked user');
  });

  test('unable to unlike - user not liked', async () => {
    const { status, body } = await request(app())
      .put(`${config.apiRoot}/user/${likedUser.id}/unlike`)
      .set({ Accept: 'application/json', Authorization: `Bearer ${userJwtToken}` })
      .send();

    expect(status).toBe(500);
    expect(typeof body).toBe('object');
    expect(typeof body.error).toBe('string');
    expect(body.error).toEqual('You did not like this user!');
  });

  test('unable to unlike non existent user', async () => {
    const nonExistentUserId = 0;
    const { status, body } = await request(app())
      .put(`${config.apiRoot}/user/${nonExistentUserId}/unlike`)
      .set({ Accept: 'application/json', Authorization: `Bearer ${userJwtToken}` })
      .send();

    expect(status).toBe(500);
    expect(typeof body).toBe('object');
    expect(typeof body.error).toBe('string');
    expect(body.error).toEqual('You did not like this user!');
  });

  test('unable to unlike yourself', async () => {
    const { status, body } = await request(app())
      .put(`${config.apiRoot}/user/${testUser.id}/unlike`)
      .set({ Accept: 'application/json', Authorization: `Bearer ${userJwtToken}` })
      .send();

    expect(status).toBe(500);
    expect(typeof body).toBe('object');
    expect(typeof body.error).toBe('string');
    expect(body.error).toEqual('You cannot unlike yourself!');
  });
});

describe('GET /user/:id', () => {
  test('successfully retrieve user info - authenticated', async () => {
    const { status, body } = await request(app())
      .get(`${config.apiRoot}/user/${likedUser.id}`)
      .set({ Accept: 'application/json', Authorization: `Bearer ${userJwtToken}` })
      .send();

    expect(status).toBe(200);
    expect(typeof body).toBe('object');
    expect(typeof body.username).toBe('string');
    expect(typeof body.likedCount).toBe('number');
    expect(body).toEqual({ username: likedUser.username, likedCount: 0 });
  });

  test('successfully retrieve user info - unauthenticated', async () => {
    const { status, body } = await request(app())
      .get(`${config.apiRoot}/user/${testUser.id}`)
      .set({ Accept: 'application/json' })
      .send();

    expect(status).toBe(200);
    expect(typeof body).toBe('object');
    expect(typeof body.username).toBe('string');
    expect(typeof body.likedCount).toBe('number');
    expect(body).toEqual({ username: testUser.username, likedCount: 0 });
  });

  test('unable to retrieve user info of non existent user', async () => {
    const nonExistentUserId = 0;
    const { status, body } = await request(app())
      .get(`${config.apiRoot}/user/${nonExistentUserId}`)
      .set({ Accept: 'application/json' })
      .send();

    expect(status).toBe(404);
    expect(typeof body).toBe('object');
    expect(body).toEqual({});
  });
});

describe('GET /most-liked', () => {
  test('successfully retrieve user info', async () => {
    const { status, body } = await request(app())
      .get(`${config.apiRoot}/most-liked`)
      .set({ Accept: 'application/json', Authorization: `Bearer ${userJwtToken}` })
      .send();

    expect(status).toBe(200);
    expect(Array.isArray(body)).toBe(true);
  });

  test('returns correct data', async () => {
    const signedUpUserUsername = 'testuser';
    const { status, body } = await request(app())
      .get(`${config.apiRoot}/most-liked`)
      .set({ Accept: 'application/json' })
      .send();

    const usersCount = 3;
    expect(status).toBe(200);
    expect(body).toHaveLength(usersCount);
    expect(body[0].likedCount).toBeGreaterThanOrEqual(body[body.length - 1].likedCount);
    expect(body).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          username: testUser.username,
          likedCount: 0,
        }),
        expect.objectContaining({
          username: likedUser.username,
          likedCount: 0,
        }),
        expect.objectContaining({
          username: signedUpUserUsername,
          likedCount: 0,
        }),
      ]),
    );
  });

  test('array returned has correct format', async () => {
    const { status, body } = await request(app())
      .get(`${config.apiRoot}/most-liked`)
      .set({ Accept: 'application/json' })
      .send();

    expect(status).toBe(200);
    expect(body).toEqual(
      expect.arrayContaining([
        expect.objectContaining({
          id: expect.any(Number),
          username: expect.any(String),
          firstName: expect.any(String),
          lastName: expect.any(String),
          likedCount: expect.any(Number),
        }),
      ]),
    );
  });
});
