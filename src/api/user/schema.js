import Joi from 'joi';

class UserSchema {
  constructor() {
    this.initializeSchema();
  }

  get updatePassword() {
    return this._updatePassword;
  }

  get likeUser() {
    return this._likeUser;
  }

  get unlikeUser() {
    return this._unlikeUser;
  }

  get likesInfo() {
    return this._likesInfo;
  }

  get mostLiked() {
    return this._mostLiked;
  }

  initializeSchema() {
    this._updatePassword = {
      body: {
        currentPassword: Joi.string().required().regex(/^[a-zA-Z0-9]{3,30}$/),
        newPassword: Joi.string().required().regex(/^[a-zA-Z0-9]{3,30}$/),
        newPasswordConfirm: Joi.string().required().regex(/^[a-zA-Z0-9]{3,30}$/).valid(Joi.ref('newPassword')),
      },
    };

    this._likeUser = {
      params: {
        id: Joi.number().integer().min(0).required(),
      },
    };

    this._unlikeUser = {
      params: {
        id: Joi.number().integer().min(0).required(),
      },
    };

    this._likesInfo = {
      params: {
        id: Joi.number().integer().min(0).required(),
      },
    };

    this._mostLiked = {
      query: {
        userId: Joi.number().integer().min(0).optional(),
      },
    };
  }
}

export default new UserSchema();
