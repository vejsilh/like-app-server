import Response from '../../services/response';
import User from '../../db/models/user';
import log from '../../services/logger';

export default class UserController {
  constructor(userRepository, likeRepository) {
    this.userRepository = userRepository;
    this.likeRepository = likeRepository;
  }

   me = async (req, res) => {
     const userId = req.user.id;

     try {
       const user = await this.userRepository.findById(userId);

       log.info({ req }, 'Retrieved currently logged user info');
       return Response.success(res, user.view());
     } catch (err) {
       log.error({ req, err }, 'Failed to Retrieve currently logged user info!');
       return Response.internalServerError(res, err);
     }
   }

   updatePassword = async (req, res) => {
     const userId = req.user.id;
     const { currentPassword, newPassword } = req.body;

     try {
       const user = await this.userRepository.findById(userId);
       const isLegitPassword = await User.authenticate(currentPassword, user.password);

       if (!isLegitPassword) {
         log.info({ req }, 'User password update failed!');
         return Response.internalServerError(res, new Error('Current password is incorrect!'));
       }

       const newPasswordHash = await User.hashPassword(newPassword);
       await this.userRepository.updateById(userId, { password: newPasswordHash });

       log.info({ req }, 'User updated password');
       return Response.success(res, { message: 'Password sucessufuly changed' });
     } catch (err) {
       log.info({ req, err }, 'User password update failed!');
       return Response.internalServerError(res, err);
     }
   }

   likeUser = async (req, res) => {
     const targetUserId = req.params.id;
     const currentUserId = req.user.id;

     if (parseInt(targetUserId, 10) === currentUserId) {
       log.error({ req }, 'Liking user failed!');
       return Response.internalServerError(res, new Error('You cannot like yourself!'));
     }

     const likeData = {
       likedBy: currentUserId,
       likedUser: targetUserId,
     };

     let transaction = null;
     try {
       transaction = await this.userRepository.getTransaction();
       await this.likeRepository.create(likeData, transaction);
       await this.userRepository.incrementLikedCount(targetUserId, transaction);
       await transaction.commit();

       log.info({ req }, 'User sucessufuly liked');
       return Response.success(res, { message: 'Sucessufuly liked user' });
     } catch (err) {
       log.error({ req, err }, 'Liking user failed!');
       transaction.rollback();
       return Response.internalServerError(res, err);
     }
   }

   unlikeUser = async (req, res) => {
     const targetUserId = req.params.id;
     const currentUserId = req.user.id;

     if (parseInt(targetUserId, 10) === currentUserId) {
       log.error({ req }, 'Unliking user failed!');
       return Response.internalServerError(res, new Error('You cannot unlike yourself!'));
     }

     const likeData = {
       likedBy: currentUserId,
       likedUser: targetUserId,
     };

     let transaction = null;
     try {
       transaction = await this.userRepository.getTransaction();
       await this.likeRepository.delete(likeData, transaction);
       await this.userRepository.decrementLikedCount(targetUserId, transaction);
       await transaction.commit();

       log.info({ req }, 'User sucessufuly unliked');
       return Response.success(res, { message: 'Sucessufuly unliked user' });
     } catch (err) {
       log.error({ req, err }, 'Unliking user failed!');
       transaction.rollback();
       return Response.internalServerError(res, err);
     }
   }

   likesInfo = async (req, res) => {
     const userId = parseInt(req.params.id, 10);

     try {
       const user = await this.userRepository.findById(userId);

       if (!user) {
         log.error({ req }, 'Failed to retrieve user likes info!');
         return Response.notFound(res);
       }

       const userLikesInfo = {
         username: user.username,
         likedCount: user.likedCount,
       };

       log.info({ req, userLikesInfo }, 'User likes info retrieved');
       return Response.success(res, userLikesInfo);
     } catch (err) {
       log.error({ req, err }, 'Failed to retrieve user likes info!');
       return Response.internalServerError(res, err);
     }
   }

   mostLiked = async (req, res) => {
     const { userId } = req.query;

     try {
       const all = await this.userRepository.getMostLiked(userId);

       log.info({ req, all }, 'retrieved most liked users');
       return Response.success(res, all);
     } catch (err) {
       log.error({ req, err }, 'Failed to retrieve most liked users!');
       return Response.internalServerError(res, err);
     }
   }
}
