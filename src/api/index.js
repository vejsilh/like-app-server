import { Router } from 'express';
import auth from './auth';
import user from './user';

const router = new Router();

router.use('/', user);
router.use('/', auth);

export default router;
