

const seed = {
  up: queryInterface => queryInterface.bulkInsert('Users', [{
    id: 1,
    firstName: 'John',
    lastName: 'Doe',
    username: 'johndoe',
    password: '$2b$10$pF5sREwT6FLo7kiQoCwDz.OUFa10is6oBLWCF6ukFUwDLwgrYFOSK', // 'demo'
    likedCount: 1,
    createdAt: new Date(),
    updatedAt: new Date(),
  }, {
    id: 2,
    firstName: 'Grace',
    lastName: 'Monroe',
    username: 'gracemo',
    password: '$2b$10$pF5sREwT6FLo7kiQoCwDz.OUFa10is6oBLWCF6ukFUwDLwgrYFOSK', // 'demo'
    likedCount: 1,
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    id: 3,
    firstName: 'Sabina',
    lastName: 'Joyner',
    username: 'sabinajo',
    password: '$2b$10$pF5sREwT6FLo7kiQoCwDz.OUFa10is6oBLWCF6ukFUwDLwgrYFOSK', // 'demo'
    likedCount: 1,
    createdAt: new Date(),
    updatedAt: new Date(),
  }]),
  down: queryInterface => queryInterface.bulkDelete('Users', null, {}),
};

export default seed;
