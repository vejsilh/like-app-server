const seed = {
  up: queryInterface => queryInterface.bulkInsert('Likes', [{
    likedBy: 1,
    likedUser: 2,
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    likedBy: 1,
    likedUser: 3,
    createdAt: new Date(),
    updatedAt: new Date(),
  },
  {
    likedBy: 3,
    likedUser: 1,
    createdAt: new Date(),
    updatedAt: new Date(),
  }], {}),
  down: queryInterface => queryInterface.bulkDelete('Likes', null, {}),
};

export default seed;
