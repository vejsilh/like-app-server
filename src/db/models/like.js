import { Model } from 'sequelize';

class Like extends Model {
  static init(sequelize) {
    return super.init({ }, { sequelize });
  }

  static associate(models) {
    this.belongsTo(models.User, { foreignKey: 'likedBy' });
    this.belongsTo(models.User, { foreignKey: 'likedUser' });
  }
}

export default Like;
