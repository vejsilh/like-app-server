import Sequelize from 'sequelize';
import config from '../../config';

import User from './user';
import Like from './like';

const sequelize = new Sequelize(
  config.dbConfig.database,
  config.dbConfig.username,
  config.dbConfig.password,
  config.dbConfig,
);

const models = {
  User: User.init(sequelize, Sequelize),
  Like: Like.init(sequelize, Sequelize),
};

Object.values(models)
  .filter(model => typeof model.associate === 'function')
  .forEach(model => model.associate(models));

const db = {
  ...models,
  sequelize,
};

export default db;
