import bcrypt from 'bcrypt';
import { Model } from 'sequelize';

class User extends Model {
  static init(sequelize, DataTypes) {
    return super.init(
      {
        username: {
          type: DataTypes.STRING,
          validate: {
            len: [2, 50],
          },
          allowNull: false,
        },
        firstName: {
          type: DataTypes.STRING,
          validate: {
            len: [2, 50],
          },
          allowNull: false,
        },
        lastName: {
          type: DataTypes.STRING,
          validate: {
            len: [2, 50],
          },
          allowNull: false,
        },
        password: {
          type: DataTypes.STRING,
          allowNull: false,
        },
        likedCount: {
          type: DataTypes.INTEGER,
          allowNull: false,
          defaultValue: 0,
        },
      },
      {
        hooks: {
          beforeCreate: async (user) => {
            try {
              const hashedPassword = await User.hashPassword(user.password);
              // eslint-disable-next-line no-param-reassign
              user.password = hashedPassword;
            } catch (err) {
              sequelize.Promise.reject(err);
            }
          },
        },
        sequelize,
      },
    );
  }

  view() {
    return {
      id: this.id,
      firstName: this.firstName,
      lastName: this.lastName,
      username: this.username,
      likedCount: this.likedCount,
    };
  }

  static async hashPassword(password) {
    try {
      return await bcrypt.hash(password, 10);
    } catch (err) {
      throw err;
    }
  }

  static async authenticate(password, hashedPassword) {
    try {
      return await bcrypt.compare(password, hashedPassword);
    } catch (err) {
      throw err;
    }
  }

  static associate(models) {
    this.hasMany(models.Like, { as: 'likedUsers', foreignKey: 'likedBy' });
    this.hasMany(models.Like, { as: 'likedBy', foreignKey: 'likedUser' });
  }
}

export default User;
