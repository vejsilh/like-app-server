class BaseRepository {
  constructor(appDb) {
    this.db = appDb;
  }

  async getTransaction() {
    try {
      return await this.db.sequelize.transaction();
    } catch (err) {
      throw err;
    }
  }
}

export default BaseRepository;
