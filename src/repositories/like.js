import BaseRepository from './base';

class LikeRepository extends BaseRepository {
  async create(likeData, transaction) {
    try {
      const [likeItem, created] = await this.db.Like.findOrCreate({ where: likeData, transaction });
      if (!created) {
        throw new Error('You already liked this user');
      }
      return likeItem;
    } catch (err) {
      throw err;
    }
  }

  async delete(likeData, transaction) {
    try {
      const affectedRows = await this.db.Like.destroy({ where: likeData }, { transaction });
      if (!affectedRows) {
        throw new Error('You did not like this user!');
      }
      return Boolean(affectedRows);
    } catch (err) {
      throw err;
    }
  }
}

export default LikeRepository;
