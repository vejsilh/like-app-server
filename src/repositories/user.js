import Like from '../db/models/like';
import BaseRepository from './base';

class UserRepository extends BaseRepository {
  async findById(id) {
    try {
      const user = await this.db.User.findOne({ where: { id } });
      return user;
    } catch (err) {
      throw err;
    }
  }

  async findByUsername(username) {
    try {
      const user = await this.db.User.findOne({ where: { username } });
      return user;
    } catch (err) {
      throw err;
    }
  }

  async create(userData) {
    try {
      const newUser = await this.db.User.create(userData);
      return newUser;
    } catch (err) {
      throw err;
    }
  }

  async updateById(id, attributes) {
    try {
      const updatedUser = await this.db.User.update(attributes, { where: { id } });
      return updatedUser;
    } catch (err) {
      throw err;
    }
  }

  async incrementLikedCount(targetUserId, transaction) {
    try {
      const likedFieldName = 'likedCount';
      return await this.db.User.increment(likedFieldName, { where: { id: targetUserId }, transaction });
    } catch (err) {
      throw err;
    }
  }

  async decrementLikedCount(targetUserId, transaction) {
    try {
      const likedFieldName = 'likedCount';
      return await this.db.User.decrement(likedFieldName, { where: { id: targetUserId }, transaction });
    } catch (err) {
      throw err;
    }
  }

  async getMostLiked(currentUserId) {
    try {
      const whereClause = {};
      const selectAttributes = ['id', 'username', 'firstName', 'lastName', 'likedCount'];
      if (currentUserId) {
        whereClause.likedBy = currentUserId;
        selectAttributes.push([this.db.sequelize.fn('COUNT', this.db.sequelize.col('likedBy.id')), 'likedByMe']);
      }

      const allUsers = await this.db.User.findAll({
        attributes: selectAttributes,
        include: [{
          model: Like,
          as: 'likedBy',
          where: whereClause,
          required: false,
          attributes: [],
        }],
        group: ['User.id', 'User.username', 'User.firstName', 'User.lastName', 'User.likedCount', 'likedBy.id'],
        order: [['likedCount', 'DESC']],
      });
      return allUsers;
    } catch (err) {
      throw err;
    }
  }
}

export default UserRepository;
