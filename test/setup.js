/* eslint-disable no-restricted-syntax */
import models from '../src/db/models';

export default async () => {
  const sequelizeObjKey = 'sequelize';
  for (const key of Object.keys(models)) {
    if (key !== sequelizeObjKey) {
      // eslint-disable-next-line no-await-in-loop
      await models[key].destroy({ where: {}, force: true });
    }
  }
};
